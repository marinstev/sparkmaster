# -*- coding: utf-8 -*-
"""
Created on Fri Mar 02 13:35:57 2018

@author: marin
"""
from pyspark import SparkContext, SQLContext
from pyspark.mllib.classification import LogisticRegressionWithLBFGS, SVMWithSGD
from pyspark.mllib.regression import LabeledPoint
import csv
import string
import re
import numpy as np
import gzip
from gensim.models import Word2Vec
from bs4 import BeautifulSoup
import cPickle
from nltk.tokenize import RegexpTokenizer
from nltk.stem.wordnet import WordNetLemmatizer
from pyspark.mllib.evaluation import MulticlassMetrics


lemmer = WordNetLemmatizer()
alpha_tokenizer = RegexpTokenizer('[A-Za-z]\w+')

def cleanText(text):
    text = re.sub(r'http[s]?:\/\/.*[\W]*', '', text, flags=re.MULTILINE)# remove urls
    text = re.sub(r'@[\w]*', '', text, flags=re.MULTILINE) # remove the @twitter mentions 
    text = re.sub(r'RT.*','', text, flags=re.MULTILINE)  # delete the retweets
    text = text.replace('\n', ' ') #replace enters
    text = filter(None, text) #remove empty tweets
    text = text.lower()
    text = re.sub(r'http\S+', '', text)
    text = text.replace("via", " ")
    text = text.replace("$", "dollar")
    text = BeautifulSoup(text, "lxml").get_text()
    text = "".join([ch for ch in text if ch not in string.punctuation])
    tokens = text.split(" ")
    cleaned = []
    for item in tokens:
        if not item.isdigit():  # item not in stop
            item = "".join([e for e in item if e.isalnum()])
            if item:
                cleaned.append(item)
    if cleaned:
        return cleaned
    else:
        return [""]
		
def determineWord2VecFeatureVector(text, label, word2vecModel, numberOfFeatures):
    index2word_set = set(word2vecModel.wv.index2word)
    featureVec = np.zeros((numberOfFeatures,), dtype="float32")
    nwords = 0.0      
    text = text.decode('unicode-escape')   
    cleanedText = cleanText(text)
    for word in cleanedText:
        word = lemmer.lemmatize(word)
        if word and word in index2word_set:  
            nwords = nwords + 1.0
            featureVec = np.add(featureVec, word2vecModel[word])
    featureVec = np.divide(featureVec, nwords)
    featureVec = np.nan_to_num(featureVec)
    return LabeledPoint(float(label), featureVec), text

def predictMulticlassBinaryClassificators (dataRecord, model_pol_fin, model_pol_sports, model_pol_tech, model_pol_ent, model_pol_crime,
                    model_fin_sports, model_fin_tech, model_fin_ent, model_fin_crime, model_sports_tech,
                    model_sports_ent, model_sports_crime, model_tech_ent, model_tech_crime, model_ent_crime,
                    labels):
    lab_count = np.zeros(6, dtype="int32")
    if model_pol_fin.predict(dataRecord[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[1] += 1
    if model_pol_sports.predict(dataRecord[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[2] += 1
    if model_pol_tech.predict(dataRecord[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[3] += 1
    if model_pol_ent.predict(dataRecord[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[4] += 1
    if model_pol_crime.predict(dataRecord[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[5] += 1
    if model_fin_sports.predict(dataRecord[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[2] += 1
    if model_fin_tech.predict(dataRecord[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[3] += 1
    if model_fin_ent.predict(dataRecord[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[4] += 1
    if model_fin_crime.predict(dataRecord[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[5] += 1
    if model_sports_tech.predict(dataRecord[0].features) == 1:
        lab_count[2] += 1
    else:
        lab_count[3] += 1
    if model_sports_ent.predict(dataRecord[0].features) == 1:
        lab_count[2] += 1
    else:
        lab_count[4] += 1
    if model_sports_crime.predict(dataRecord[0].features) == 1:
        lab_count[2] += 1
    else:
        lab_count[5] += 1
    if model_tech_ent.predict(dataRecord[0].features) == 1:
        lab_count[3] += 1
    else:
        lab_count[4] += 1
    if model_tech_crime.predict(dataRecord[0].features) == 1:
        lab_count[3] += 1
    else:
        lab_count[5] += 1
    if model_ent_crime.predict(dataRecord[0].features) == 1:
        lab_count[4] += 1
    else:
        lab_count[5] += 1
    # print(lab_count)
    correct = 0
    actual_label = labels[int(dataRecord[0].label)]
    if int(np.sum(lab_count)) == 0:
        return correct, "Other", actual_label, dataRecord[1]
    else:
        args = np.argwhere(lab_count == np.amax(lab_count))
        argl = args.flatten().tolist()
        pred_label = [labels[i] for i in argl]
        if dataRecord[0].label in argl:
            correct = 1
        return correct, pred_label, actual_label, dataRecord[1]

def saveModel(model, name):
    f = gzip.open(name + '.pkl.gz', 'wb')
    cPickle.dump(model, f)
    f.close()

def Nlabels2(line, c):
    if line[0].label == c:
        line[0].label = float(1)
    else:
        line[0].label = float(0.0)
    return line

def calcAccuracy(rdd):
    nrow = rdd.count()
    print('Test data size: ', nrow)
    if nrow > 0:
        print("ACCURACY", rdd.filter(lambda x: x[0] == 1).count()/float(nrow))

def trainAndPredictMulticlassBinaryClassificators(training, test, classificator, labels):
    pol_fin = training.filter(lambda p: (p[0].label == 0.0 or p[0].label == 1.0))
    pol_sports = training.filter(lambda p: (p[0].label == 0.0 or p[0].label == 2.0))
    pol_tech = training.filter(lambda p: (p[0].label == 0.0 or p[0].label == 3.0))
    pol_ent = training.filter(lambda p: (p[0].label == 0.0 or p[0].label == 4.0))
    pol_crime = training.filter(lambda p: (p[0].label == 0.0 or p[0].label == 5.0))
    fin_sports = training.filter(lambda p: (p[0].label == 1.0 or p[0].label == 2.0))
    fin_tech = training.filter(lambda p: (p[0].label == 1.0 or p[0].label == 3.0))
    fin_ent = training.filter(lambda p: (p[0].label == 1.0 or p[0].label == 4.0))
    fin_crime = training.filter(lambda p: (p[0].label == 1.0 or p[0].label == 5.0))
    sports_tech = training.filter(lambda p: (p[0].label == 2.0 or p[0].label == 3.0))
    sports_ent = training.filter(lambda p: (p[0].label == 2.0 or p[0].label == 4.0))
    sports_crime = training.filter(lambda p: (p[0].label == 2.0 or p[0].label == 5.0))
    tech_ent = training.filter(lambda p: (p[0].label == 3.0 or p[0].label == 4.0))
    tech_crime = training.filter(lambda p: (p[0].label == 3.0 or p[0].label == 5.0))
    ent_crime = training.filter(lambda p: (p[0].label == 4.0 or p[0].label == 5.0))
    
    p = lambda j: Nlabels2(j, 0.0)
    f = lambda j: Nlabels2(j, 1.0)
    s = lambda j: Nlabels2(j, 2.0)
    t = lambda j: Nlabels2(j, 3.0)
    e = lambda j: Nlabels2(j, 4.0)
    c = lambda j: Nlabels2(j, 5.0)
    
    pol_fin = pol_fin.map(p)
    pol_sports = pol_sports.map(p)
    pol_tech = pol_tech.map(p)
    pol_ent = pol_ent.map(p)
    pol_crime = pol_crime.map(p)
    fin_sports = fin_sports.map(f)
    fin_tech = fin_tech.map(f)
    fin_ent = fin_ent.map(f)
    fin_crime = fin_crime.map(f)
    sports_tech = sports_tech.map(s)
    sports_ent = sports_ent.map(s)
    sports_crime = sports_crime.map(s)
    tech_ent = tech_ent.map(t)
    tech_crime = tech_crime.map(t)
    ent_crime = ent_crime.map(e)
    
    if (classificator == 'logisticregression'):
        model_pol_fin = LogisticRegressionWithLBFGS.train(pol_fin.map(lambda x: x[0]))
        model_pol_sports  = LogisticRegressionWithLBFGS.train(pol_sports.map(lambda x: x[0]))
        model_pol_tech = LogisticRegressionWithLBFGS.train(pol_tech.map(lambda x: x[0]))
        model_pol_ent = LogisticRegressionWithLBFGS.train(pol_ent.map(lambda x: x[0]))
        model_pol_crime = LogisticRegressionWithLBFGS.train(pol_crime.map(lambda x: x[0]))
        model_fin_sports = LogisticRegressionWithLBFGS.train(fin_sports.map(lambda x: x[0]))
        model_fin_tech = LogisticRegressionWithLBFGS.train(fin_tech.map(lambda x: x[0]))
        model_fin_ent = LogisticRegressionWithLBFGS.train(fin_ent.map(lambda x: x[0]))
        model_fin_crime = LogisticRegressionWithLBFGS.train(fin_crime.map(lambda x: x[0]))
        model_sports_tech = LogisticRegressionWithLBFGS.train(sports_tech.map(lambda x: x[0]))
        model_sports_ent = LogisticRegressionWithLBFGS.train(sports_ent.map(lambda x: x[0]))
        model_sports_crime = LogisticRegressionWithLBFGS.train(sports_crime.map(lambda x: x[0]))
        model_tech_ent = LogisticRegressionWithLBFGS.train(tech_ent.map(lambda x: x[0]))
        model_tech_crime = LogisticRegressionWithLBFGS.train(tech_crime.map(lambda x: x[0]))
        model_ent_crime = LogisticRegressionWithLBFGS.train(ent_crime.map(lambda x: x[0]))
    else:
        if (classificator == 'svm'):
			model_pol_fin = SVMWithSGD.train(pol_fin.map(lambda x: x[0]))
			model_pol_sports  = SVMWithSGD.train(pol_sports.map(lambda x: x[0]))
			model_pol_tech = SVMWithSGD.train(pol_tech.map(lambda x: x[0]))
			model_pol_ent = SVMWithSGD.train(pol_ent.map(lambda x: x[0]))
			model_pol_crime = SVMWithSGD.train(pol_crime.map(lambda x: x[0]))
			model_fin_sports = SVMWithSGD.train(fin_sports.map(lambda x: x[0]))
			model_fin_tech = SVMWithSGD.train(fin_tech.map(lambda x: x[0]))
			model_fin_ent = SVMWithSGD.train(fin_ent.map(lambda x: x[0]))
			model_fin_crime = SVMWithSGD.train(fin_crime.map(lambda x: x[0]))
			model_sports_tech = SVMWithSGD.train(sports_tech.map(lambda x: x[0]))
			model_sports_ent = SVMWithSGD.train(sports_ent.map(lambda x: x[0]))
			model_sports_crime = SVMWithSGD.train(sports_crime.map(lambda x: x[0]))
			model_tech_ent = SVMWithSGD.train(tech_ent.map(lambda x: x[0]))
			model_tech_crime = SVMWithSGD.train(tech_crime.map(lambda x: x[0]))
			model_ent_crime = SVMWithSGD.train(ent_crime.map(lambda x: x[0]))
       
    testOutput = test.map(lambda dataRecord: predictMulticlassBinaryClassificators(dataRecord,
                                                 model_pol_fin,
                                                 model_pol_sports,
                                                 model_pol_tech,
                                                 model_pol_ent,
                                                 model_pol_crime,
                                                 model_fin_sports,
                                                 model_fin_tech,
                                                 model_fin_ent,
                                                 model_fin_crime,
                                                 model_sports_tech,
                                                 model_sports_ent,
                                                 model_sports_crime,
                                                 model_tech_ent,
                                                 model_tech_crime,
                                                 model_ent_crime,
                                                 labels
                                                 ))
    #print testOutput.take(50)
    print ("ACCURACY BINARY")
    acc = 1.0 * testOutput.filter(lambda pl: pl[1] == pl[2]).count() / testOutput.count()
    #testOutput.foreachRDD(calcAccuracy)
    print ("ACCURACY BINARY " + str(acc))
    
    path = "C:\\SparkMaster\\ClassificationModels\\"
    saveModel(model_pol_fin, path + "pol_fin_" + classificator)
    saveModel(model_pol_sports,  path + "pol_sports_"+ classificator)
    saveModel(model_pol_tech,  path + "pol_tech_" + classificator)
    saveModel(model_pol_ent,  path + "pol_ent_" + classificator)
    saveModel(model_pol_crime,  path + "pol_crime_" + classificator)
    saveModel(model_fin_sports,  path + "fin_sports_" + classificator)
    saveModel(model_fin_tech,  path + "fin_tech_" + classificator)
    saveModel(model_fin_ent,  path + "fin_ent_" + classificator)
    saveModel(model_fin_crime,  path + "fin_crime_" + classificator)
    saveModel(model_sports_tech,   path + "sports_tech_" + classificator)
    saveModel(model_sports_ent,  path + "sports_ent_" + classificator)
    saveModel(model_sports_crime,  path + "sports_crime_" + classificator)
    saveModel(model_tech_ent,  path + "tech_ent_" + classificator)
    saveModel(model_tech_crime,  path + "tech_crime_" + classificator)
    saveModel(model_ent_crime,  path + "ent_crime_" + classificator)
	
def loadData(path):
    data = sc.textFile(path)
    data = data.mapPartitions(lambda x: csv.reader(x, delimiter='`', quotechar='|'))
    return data
    
def loadWord2VecModel(path):
    word2vec_model = Word2Vec.load(path)
    return word2vec_model

def trainMulticlassClassificator(modelPath, trainingData, numberOfClasses):
    model = LogisticRegressionWithLBFGS.train(trainingData.map(lambda x: x[0]), numClasses=numberOfClasses)
    saveModel(model, modelPath)
    return model


def predictMulticlassClassificatorWithMetrics (model, test, training):
    predictionAndLabels = test.map(lambda lp: (float(model.predict(lp[0].features)), lp[0].label))
    
    print('=========================== TRAINING START =========================================')
    print predictionAndLabels.take(50)
    print('=========================== TRAINING END =========================================')
    
    # Instantiate metrics object
    metrics = MulticlassMetrics(predictionAndLabels)
    print('=========================== MULTICLASS =========================================')
    # Overall statistics
    precision = metrics.precision()
    print('=========================== PRECISSION =========================================')
    recall = metrics.recall()
    print('=========================== RECALL =========================================')
    f1Score = metrics.fMeasure()
    print('=========================== FMEASURE =========================================')
    print("Summary Stats")
    print("Precision = %s" % precision)
    print("Recall = %s" % recall)
    print("F1 Score = %s" % f1Score)
	
    print metrics.accuracy
    print metrics.confusionMatrix().toArray()
    
    # Weighted stats
    print("Weighted recall = %s" % metrics.weightedRecall)
    print("Weighted precision = %s" % metrics.weightedPrecision)
    print("Weighted F(1) Score = %s" % metrics.weightedFMeasure())
    print("Weighted F(0.5) Score = %s" % metrics.weightedFMeasure(beta=0.5))
    print("Weighted false positive rate = %s" % metrics.weightedFalsePositiveRate)
	
if __name__ == '__main__':
    sc = SparkContext('local[*]', appName="MultipleModels")
    sqlContext = SQLContext(sc)
    
    labels = ['Politics', 'Finance', 'Sports', 'Sci&Tech', 'Entertainment', 'Crime']
    
    num_features = 300
    word2vec_model = loadWord2VecModel("C:\\SparkMaster\\FeatureModels\\lem_model")
    
    training_partitions = loadData("C:\\SparkMaster\\LabeledData\\batch_data\\*.csv")    
    f = lambda j: determineWord2VecFeatureVector(j, word2vec_model, num_features)
    training = training_partitions.map(f).cache()
    
    test_partitions = loadData("C:\\SparkMaster\\LabeledData\\batch_data_test\\*.csv")    
    f = lambda j: determineWord2VecFeatureVector(j, word2vec_model, num_features)
    test = test_partitions.map(f).cache()
 
    print('================= BINARY CLASSIFICATION LOGISTIC REGRESSION =================')
    trainAndPredictMulticlassBinaryClassificators (training, test, "logisticregression", labels)
    
    print('================= BINARY CLASSIFICATION SVM =================')
    trainAndPredictMulticlassBinaryClassificators (training, test, "svm", labels)
    
    model = trainMulticlassClassificator("C:\\SparkMaster\\ClassificationModels\\MulticlassModel", training, 6)
    predictMulticlassClassificatorWithMetrics(model, test, training)
    
    
      