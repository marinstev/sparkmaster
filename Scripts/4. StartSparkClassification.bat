@echo off 
cd /D %temp%
FOR /D /R %%X IN (spark-*) DO RD /S /Q "%%X"
FOR /D /R %%X IN (blockmgr-*) DO RD /S /Q "%%X"
TIMEOUT /T 2
cd C:\Spark\bin
spark-submit --packages org.mongodb.spark:mongo-spark-connector_2.10:2.2.1 --jars C:\SparkMaster\AdditionalFiles\spark-streaming-kafka-0-8-assembly_2.11-2.2.1.jar C:\SparkMaster\SparkStreamingTweetsClassification.py >> C:\SparkMaster\Sparklog\log.txt
pause