from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.ml.feature import IDF, Tokenizer
from pyspark.ml.feature import CountVectorizer
from pyspark.ml.feature import StopWordsRemover
import csv
import re
import string
from nltk.stem.wordnet import WordNetLemmatizer

lemmer = WordNetLemmatizer()

from nltk.tokenize import RegexpTokenizer

alpha_tokenizer = RegexpTokenizer('[A-Za-z]\w+')

def lemmatizeWords(tokens):
    for index, word in enumerate(tokens): 
        lower = word.lower();
        tokens[index] = lemmer.lemmatize(lower)
    return tokens

def cleanText(text):
    cleaned_text = re.sub(r'http[s]?:\/\/.*[\W]*', '', text, flags=re.MULTILINE)# remove urls
    cleaned_text = re.sub(r'@[\w]*', '', cleaned_text, flags=re.MULTILINE) # remove the @twitter mentions 
    cleaned_text = re.sub(r'RT.*','', cleaned_text, flags=re.MULTILINE)  # delete the retweets
    cleaned_text = cleaned_text.replace('\n', ' ') #replace enters
    cleaned_text = cleaned_text.translate(None, string.punctuation)
    cleaned_text = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",cleaned_text).split())
    return cleaned_text
	
def getBagOfWordsModelTrainingData(sparkContext, path):
    training_initial = sparkContext.textFile(path)
    training_partitions = training_initial.mapPartitions(lambda x: csv.reader(x, delimiter='`', quotechar='|'))
    training = training_partitions.map(lambda row: (row[1],cleanText(row[0]))).cache()
    return training

def createDataFrameData(sparkSession, data):
    dataFrame = sparkSession.createDataFrame(data)
    dataFrame.show()
    return dataFrame

def tokenizeDataFrame(dataFrame):
    tokenizer = Tokenizer(inputCol="_2", outputCol="words")
    tokenizedData = tokenizer.transform(dataFrame)
    return tokenizedData

def removeStopWords(tokenizedData):
    remover = StopWordsRemover(inputCol="words", outputCol="wordsremoved")
    wordsDataRemoved = remover.transform(tokenizedData)
    wordsDataRemoved = wordsDataRemoved.filter("wordsremoved is not NULL");
    return wordsDataRemoved
	
def createAndMapDataToDataFrame(wordsDataRemoved):
    wordsDataRemoved = wordsDataRemoved.rdd.map(lambda el: (el["_1"],el["_2"],el["words"], lemmatizeWords(el["wordsremoved"])))
    wordsDataRemoved = spark.createDataFrame(wordsDataRemoved)
    wordsDataRemoved.show()
    return wordsDataRemoved

def createCountVectorizerAndTFIDFModel(dataFrame, cvModelPath, tfIDFModelPath):
    cv = CountVectorizer(inputCol="_4", outputCol="rawFeatures", vocabSize=10000, minDF=2.0)
    featurizedData = cv.fit(dataFrame)
    featurizedData.save(cvModelPath)
    cvTransformed = featurizedData.transform(dataFrame)
    
    idf = IDF(inputCol="rawFeatures", outputCol="features")
    idfModel = idf.fit(cvTransformed)
    rescaledData = idfModel.transform(cvTransformed)    
    idfModel.write().overwrite().save(tfIDFModelPath)
	
if __name__ == '__main__':
    sc = SparkContext('local[*]', appName="BagOfWords")

    training = getBagOfWordsModelTrainingData(sc, "C:\\SparkMaster\\ArticlesCorpora\\articles\\*.csv")
    
    spark = SparkSession(sc)    
    dataFrame = createDataFrameData(spark, training)
    tokenizedData = tokenizeDataFrame(dataFrame)
    wordsDataRemoved = removeStopWords(tokenizedData)
    
    convertedDataFrame = createAndMapDataToDataFrame(wordsDataRemoved)
    
    cvModelPath = "C:\\SparkMaster\\FeatureModels\\cv_model"
    tfidfModelPath = "C:\\SparkMaster\\FeatureModels\\tfidf_model"
    createCountVectorizerAndTFIDFModel(convertedDataFrame, cvModelPath, tfidfModelPath)
