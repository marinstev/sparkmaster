var http = require('http'),
    fs = require('fs'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var kafka = require('kafka-node');
Consumer = kafka.Consumer,
client = new kafka.Client('localhost:2181');
consumer = new Consumer(
        client,
        [{ topic: 'predictions_topic', partition: 0 }],
        { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 }
    );

	


var postData = new Array();
consumer.on('message', function (message) {
	console.log("ondata");	
	
	var dataArray = JSON.parse(message.value);
	postData.push(dataArray);
	io.emit('data', postData);
});

var express = require('express')
var app = express()
app.use(express.static('public'))
app.get("/", function(req,resp){
  resp.sendFile(__dirname + '/index.html');
});

app.get('/history', function (req, res) {
	console.log("hit history");
	var historyData = getPredictionsHistory();
    res.send(historyData)
})

var server = http.createServer(app);
var io = require('socket.io').listen(server);  //pass a http.Server instance
server.listen(3001);

mongoose.connect('mongodb://127.0.0.1:27017/NewsTweets');
mongoose.connection.on('connected', function(){
	console.log("connected to mongo");
});
mongoose.connection.on('error', function(err){
	console.log("mongo error", err);
});
mongoose.connection.on('disconnected', function(){
	console.log("disconnected from mongo");
});

var TweetBinaryLogisticRegression = mongoose.model('NewsBinaryLogisticRegression', new Schema({},{collection:'NewsBinaryLogisticRegression'}));
var TweetBinarySVM = mongoose.model('NewsBinarySVM', new Schema({},{collection:'NewsBinarySVM'}));
var TweetMulticlass = mongoose.model('NewsMulticlass', new Schema({},{collection:'NewsMulticlass'}));
var TweetNaiveBayes = mongoose.model('NewsNaiveBayes', new Schema({},{collection:'NewsNaiveBayes'}));

var fetchDataFromMultipleCollections = function(index, classyObject, returnData,callbackFunction)
{

	if(index >= classyObject.length)
	{		
		console.log("finished recursion " + index );
		callbackFunction(returnData);
	}
	else
	{
		
		classyObject[index].classifyer.find().sort({"_4": -1}).limit(1000).exec(function(err, post)
		{
			if (!err)
			{
				console.log("got data for " + index);
				if (post) 
				{
					console.log("post exists " + post.length);
				}
				returnData.push(post);
				//call again
				fetchDataFromMultipleCollections(index+1,classyObject,returnData, callbackFunction);
			}
			else
			{
				console.log("error " + index);
				console.log(err);
				callbackFunction(returnData);
			}
		});
	}
}

var gotData = function(data){
	console.log("********* DATA HISTORY  *********");
	console.log(data);
}

var getPredictionsHistory = function()
{
	console.log("getPredictionsHistory");
	var returnData = [];	
    fetchDataFromMultipleCollections(0, ClassifyFlow, returnData, gotData)
	console.log("getPredictionsHistory end");
	return returnData;
}

function getTweetBinaryLogisticRegression()
{
	console.log(getTweetBinaryLogisticRegression);
	console.log(TweetBinaryLogisticRegression);
	return TweetBinaryLogisticRegression.find().limit(1000).exec(function(err, post)
		{
			if (!err)
			{
				console.log("got data for " + index);
				if (post) 
				{
					console.log("post exists " + post.length);
					console.log(post);
				}				
			}
			else
			{
				console.log("error " + index);
				console.log(err);
				callbackFunction(returnData);
			}
		});
}
var ClassifyFlow = [
	{"classifyer": TweetBinaryLogisticRegression},
	{"classifyer": TweetBinarySVM},
	{"classifyer": TweetMulticlass},
	{"classifyer": TweetNaiveBayes}
];

// Emit welcome message on connection
io.on('connection', function(socket) {
    // Use socket to communicate with this particular client only, sending it it's own id
	console.log("socket io started");
    socket.emit('welcome', { message: 'Welcome!', id: socket.id });

    socket.on('i am client', console.log);
});

