var socket = io();
socket.on('welcome', function(data) {
});

socket.on('error', console.error.bind(console));
socket.on('message', console.log.bind(console));

var twitter_classes = ["Politics","Finance","Sports","Sci&Tech","Entertainment","Crime"];
var classificators = ["LRB", "LRM", "SVM", "NB"]
var labelsClassificators = {};
for (i = 0; i < twitter_classes.length; i++) { 
	labelsClassificators[twitter_classes[i]] = new Array(classificators.length).fill(0);;
}
var globalSeries = [{
         type: 'column',
         name: twitter_classes[0],
        data: []
     }, {
        type: 'column',
        name: twitter_classes[1],
        data: []
    },{
         type: 'column',
         name: twitter_classes[2],
         data: []
    },{
         type: 'column',
        name: twitter_classes[3],
         data: []
     },{
         type: 'column',
         name: twitter_classes[4],
         data: []
     },{
         type: 'column',
        name: twitter_classes[5],
         data: []
     }];

var globalChart = Highcharts.chart('chartContainer', {
    title: {
        text: 'News tweets labels by classificators'
    },
    xAxis: {
        categories: classificators
    },
    labels: {
        items: [{
            html: 'Total tweets by labels',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
	series: globalSeries
   
});

var all_trows = new Array();

socket.on('data', function(data) {
	console.log("data arrived");	
	var dataCount = data.length;
	console.log("data count " + dataCount);	
	var labelsCount = twitter_classes.length;
	var classificatorsCount = classificators.length;
	
	for (i = 0; i < dataCount; i++) { 
		var tweet = data[i];	
		//label_binarylog - 0, label_multiclass - 1, label_binarysvm - 2, label_bayes - 4
		labelsClassificators[tweet.label_binarylog][0]++;
		labelsClassificators[tweet.label_multiclass][1]++;
		labelsClassificators[tweet.label_binarysvm][2]++;
		labelsClassificators[tweet.label_bayes][3]++;		
	}
	
	for (i = 0; i < labelsCount; i++) { 
		var seriesElement = {
			 type: 'column',
			name: twitter_classes[i],
			data: labelsClassificators[twitter_classes[i]]
		};
	   globalSeries[i] = seriesElement;
	}
	console.log("global series");
	console.log(globalSeries);

	globalChart.update({
    series: globalSeries
});

	populateTable(data);
	
})

function populateTable(data)
{
	if (!data) return;
	var newTrows = new Array();
	console.log(data);
	for (i = 0; i < data.length; i++) { 
	 var tweet = data[i];
	 var tweetDate = moment(tweet.created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').toDate();
	 
	//create tr and add it to tbody
	var tr = document.createElement("tr");
	var td_num = document.createElement("td");
	td_num.innerHTML = i+1;
	var td_id = document.createElement("td");
	td_id.innerHTML = tweet.id;
	var td_time = document.createElement("td");
	td_time.innerHTML = tweetDate;
	var td_content = document.createElement("td");
	td_content.innerHTML = tweet.text;
	var td_1 = document.createElement("td");
	td_1.innerHTML = tweet.label_binarylog;
	var td_2 = document.createElement("td");
	td_2.innerHTML = tweet.label_binarysvm;
	var td_3 = document.createElement("td");
	td_3.innerHTML = tweet.label_multiclass;
	var td_4 = document.createElement("td");
	td_4.innerHTML = twitter_classes[parseInt(tweet._4)];
	var td_5 = document.createElement("td");
	td_5.innerHTML = tweet.label_bayes;

	tr.appendChild(td_num);
	tr.appendChild(td_id);
	tr.appendChild(td_time);
	tr.appendChild(td_content);
	tr.appendChild(td_1);
	tr.appendChild(td_2);
	tr.appendChild(td_3);
	tr.appendChild(td_4);
	tr.appendChild(td_5);
	newTrows.push(tr);
 }
 redrawTable(newTrows);
}


socket.on('dataTmp', function(data) {
console.log("data collected", data);

});

var reorderTd = function(index, td){
	td.innerHTML = index;
}

function redrawTable(new_array){
	var tableBody = document.getElementById("table_body_id");
	//update previous
	for(var i=0; i< tableBody.children.length; i++){
		reorderTd(new_array.length+i+1, tableBody.children[i].getElementsByTagName("td")[0]);
	}
	//add new elements
	for(var i = new_array.length-1; i>=0; i--){
		tableBody.insertBefore(new_array[i], tableBody.firstElementChild);
	}
}

function getHistory()
{
		
}
 