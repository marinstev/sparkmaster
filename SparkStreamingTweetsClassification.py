# -*- coding: utf-8 -*-
"""
Created on Sat Mar 03 16:32:32 2018
Modified on Tue Mar 12 22:08:17 2019

@author: marin
"""

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
import json
import ConfigParser as configparser
import cPickle
from gensim.models import Word2Vec
import os
import sys
import gzip
from pyspark.mllib.classification import LogisticRegressionWithLBFGS, NaiveBayesModel
from pyspark.mllib.regression import LabeledPoint
import numpy as np
from bs4 import BeautifulSoup
import string
import re
from nltk.stem.wordnet import WordNetLemmatizer
import datetime
import time
from pyspark.sql import SparkSession
from pyspark.sql.types import LongType
from kafka import KafkaProducer
from pyspark.ml.feature import Tokenizer, IDFModel
from pyspark.ml.feature import CountVectorizerModel
from pyspark.ml.feature import StopWordsRemover
from pyspark.sql import SparkSession

producer = KafkaProducer(bootstrap_servers='localhost:9092')
lemmer = WordNetLemmatizer()
labels = ['Politics', 'Finance', 'Sports', 'Sci&Tech', 'Entertainment', 'Crime']

remove_spl_char_regex = re.compile('[%s]' % re.escape(string.punctuation)) # regex to remove special characters
stopwords=[u'i', u'me', u'my', u'myself', u'we', u'our', u'ours', u'ourselves', u'you', u'your', u'yours', u'yourself', u'yourselves', u'he', u'him', u'his', u'himself', u'she', u'her', u'hers', u'herself', u'it', u'its', u'itself', u'they', u'them', u'their', u'theirs', u'themselves', u'what', u'which', u'who', u'whom', u'this', u'that', u'these', u'those', u'am', u'is', u'are', u'was', u'were', u'be', u'been', u'being', u'have', u'has', u'had', u'having', u'do', u'does', u'did', u'doing', u'a', u'an', u'the', u'and', u'but', u'if', u'or', u'because', u'as', u'until', u'while', u'of', u'at', u'by', u'for', u'with', u'about', u'against', u'between', u'into', u'through', u'during', u'before', u'after', u'above', u'below', u'to', u'from', u'up', u'down', u'in', u'out', u'on', u'off', u'over', u'under', u'again', u'further', u'then', u'once', u'here', u'there', u'when', u'where', u'why', u'how', u'all', u'any', u'both', u'each', u'few', u'more', u'most', u'other', u'some', u'such', u'no', u'nor', u'not', u'only', u'own', u'same', u'so', u'than', u'too', u'very', u's', u't', u'can', u'will', u'just', u'don', u'should', u'now']

def tokenize(text):
    tokens = []
    text = text.encode('ascii', 'ignore') #to decode
    text=re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', text) # to replace url with ''
    text = remove_spl_char_regex.sub(" ",text)  # Remove special characters
    text=text.lower()

    for word in text.split():
        if word not in stopwords and word not in string.punctuation and len(word)>1 and word != '``':
                tokens.append(word)
    return tokens

def cleanSent(text):
    text = text.lower()
    text = re.sub(r'http\S+', '', text)
    text = text.replace("via", " ")
    text = text.replace("$", "dollar")
    text = BeautifulSoup(text, "lxml").get_text()
    text = "".join([ch for ch in text if ch not in string.punctuation])
    tokens = text.split(" ")
    cleaned = []
    for item in tokens:
        if not item.isdigit():  # item not in stop
            item = "".join([e for e in item if e.isalnum()])
            if item:
                cleaned.append(item)
    if cleaned:
        return cleaned
    else:
        return [""]
    
def cleanText(text):
    cleaned_text = re.sub(r'http[s]?:\/\/.*[\W]*', '', text, flags=re.MULTILINE)# remove urls
    cleaned_text = re.sub(r'@[\w]*', '', cleaned_text, flags=re.MULTILINE) # remove the @twitter mentions 
    cleaned_text = re.sub(r'RT.*','', cleaned_text, flags=re.MULTILINE)  # delete the retweets
    cleaned_text = cleaned_text.replace('\n', ' ') #replace enters
    return cleaned_text

def cleanTextTFIDF(text):
    cleaned_text = re.sub(r'http[s]?:\/\/.*[\W]*', '', text, flags=re.MULTILINE)# remove urls
    cleaned_text = re.sub(r'@[\w]*', '', cleaned_text, flags=re.MULTILINE) # remove the @twitter mentions 
    cleaned_text = re.sub(r'RT.*','', cleaned_text, flags=re.MULTILINE)  # delete the retweets
    cleaned_text = cleaned_text.replace('\n', ' ') #replace enters
    cleaned_text = cleaned_text.translate(None, string.punctuation)
    cleaned_text = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",cleaned_text).split())
    return cleaned_text

def process_sent(tokens):
    for index, word in enumerate(tokens): 
        lower = word.lower();
        tokens[index] = lemmer.lemmatize(lower)
    return tokens

def parseUnlabelledTweetsForWord2Vec(tweet, word2vec_model, num_features):
    print('===== parseUnlabelledTweetsForWord2Vec =====')
    index2word_set = set(word2vec_model.wv.index2word)
    featureVec = np.zeros((num_features,), dtype="float32")
    nwords = 0.0
    text = tweet['text']
    text = cleanText(text)
    label = 0.0
    for word in cleanSent(text):
        word = lemmer.lemmatize(word)
        if word and word in index2word_set:  # (name.upper() for name in USERNAMES)
            # if word and word not in stop and word in index2word_set:
            nwords = nwords + 1.0
            featureVec = np.add(featureVec, word2vec_model[word])
    featureVec = np.divide(featureVec, nwords)
    featureVec = np.nan_to_num(featureVec)
    return LabeledPoint(label, featureVec), tweet['text'], tweet['id'], tweet['created_at'], tweet["expectedLabel"], tweet["account"]

#COLUMNS ARE NOT GOOD!!!!!!!!!!!
def parseUnlabelledTweetsForTFIDF(rdd, modelCV, modelIDF):
    print("*********TFIDFPARSE - RDD********")
    conf = rdd.context.getConf()
    spark = getSparkSessionInstance(conf)
    print type(rdd)
    print rdd.take(10)
    tweetsDataFrame = spark.createDataFrame(rdd)
    print("*********TFIDFPARSE - DATA FRAME********")
    tokenizer = Tokenizer(inputCol="text", outputCol="words")
    tokenizedTweets = tokenizer.transform(tweetsDataFrame)
    
    remover = StopWordsRemover(inputCol="words", outputCol="wordsremoved")
    tweetsRemoved = remover.transform(tokenizedTweets)
    tweetsRemoved = tweetsRemoved.filter("wordsremoved is not NULL");
    
    print("*********TFIDFPARSE - tweetsRemoved********")
    #tweetsRemoved.show(n=10)
    tweetsRemoved = tweetsRemoved.rdd.map(lambda el: (el["id"],el["created_at"],el["text"], process_sent(el["wordsremoved"]), el["expectedLabel"], el["account"]))
    tweetsDataFrame = spark.createDataFrame(tweetsRemoved)
    tweetsCV = modelCV.transform(tweetsDataFrame)
    tweetsIDF = modelIDF.transform(tweetsCV)
    
    return tweetsIDF

def jointStream(multiInfo):
    multiclass = multiInfo._1._1;
    randomForest = multiInfo._2._1;
    return (multiclass, randomForest)

def binaryMulticlassPredict(r, model_pol_fin, model_pol_sports, model_pol_tech, model_pol_ent, model_pol_crime,
                    model_fin_sports, model_fin_tech, model_fin_ent, model_fin_crime, model_sports_tech,
                    model_sports_ent, model_sports_crime, model_tech_ent, model_tech_crime, model_ent_crime,
                    labels):
    lab_count = np.zeros(6, dtype="int32")
    if model_pol_fin.predict(r[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[1] += 1
    if model_pol_sports.predict(r[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[2] += 1
    if model_pol_tech.predict(r[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[3] += 1
    if model_pol_ent.predict(r[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[4] += 1
    if model_pol_crime.predict(r[0].features) == 1:
        lab_count[0] += 1
    else:
        lab_count[5] += 1
    if model_fin_sports.predict(r[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[2] += 1
    if model_fin_tech.predict(r[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[3] += 1
    if model_fin_ent.predict(r[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[4] += 1
    if model_fin_crime.predict(r[0].features) == 1:
        lab_count[1] += 1
    else:
        lab_count[5] += 1
    if model_sports_tech.predict(r[0].features) == 1:
        lab_count[2] += 1
    else:
        lab_count[3] += 1
    if model_sports_ent.predict(r[0].features) == 1:
        lab_count[2] += 1
    else:
        lab_count[4] += 1
    if model_sports_crime.predict(r[0].features) == 1:
        lab_count[2] += 1
    else:
        lab_count[5] += 1
    if model_tech_ent.predict(r[0].features) == 1:
        lab_count[3] += 1
    else:
        lab_count[4] += 1
    if model_tech_crime.predict(r[0].features) == 1:
        lab_count[3] += 1
    else:
        lab_count[5] += 1
    if model_ent_crime.predict(r[0].features) == 1:
        lab_count[4] += 1
    else:
        lab_count[5] += 1
    # print(lab_count)
    correct = 0
    actual_label = labels[int(r[0].label)]
    if int(np.sum(lab_count)) == 0:
        return correct, "Other", actual_label, r[1]
    else:
        args = np.argwhere(lab_count == np.amax(lab_count))
        argl = args.flatten().tolist()
        pred_label = [labels[i] for i in argl]
        if r[0].label in argl:
            correct = 1
        return pred_label, r[1], r[2], r[3], r[4], r[5]
    
def saveTweetIDsToMongo(tweetIDs):
    session = SparkSession.builder.appName("TweetIDsApp").config("spark.mongodb.input.uri", "mongodb://127.0.0.1:27017/NewsTweets.TweetIDs").config("spark.mongodb.output.uri", "mongodb://127.0.0.1:27017/NewsTweets.TweetIDs").getOrCreate()
    toSave = session.createDataFrame(tweetIDs, LongType())
    toSave.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database","NewsTweets").option("collection", "TweetIDs").save()
    
def saveToMongo(rdd, context, classificator):
    dbPredictions = context.createDataFrame(rdd)
    dbPredictions.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database","NewsTweets").option("collection", "News" + classificator).save()

def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError
    
def createJson(id, text, time, label_multiclass, label_binarylog, label_binarysvm, label_bayes):
    print('================================== createJson  ==============================================')
    data = {}
    data['id'] = id
    data['text'] = text
    data['time'] = time
    data['label_multiclass'] = label_multiclass
    data['label_binarylog'] = label_binarylog
    data['label_binarysvm'] = label_binarysvm
    data['label_bayes'] = label_bayes
    json_data = json.dumps(data)
    return json_data

def loadSparkConfigFile(path):
    config = configparser.RawConfigParser()
    config.read(path)
    
    # Path for spark source folder
    SPARK_HOME = config.get('Spark', 'SPARK_HOME')
    os.environ['SPARK_HOME'] = SPARK_HOME

    # Append pyspark to Python Path
    sys.path.append(SPARK_HOME + config.get('Spark', 'python_path'))
    sys.path.append(SPARK_HOME + config.get('Spark', 'py4j_path'))
    
def readTweetsFromKafkaStream(sparkStreamingContext, kafkaHost, consumerGroupID, topic):
    kafkaStream = KafkaUtils.createStream(ssc, 'localhost:2181', 'spark-streaming', {'twitterTopic':1})  
    parsed = kafkaStream.map(lambda v: json.loads(v[1]))
    return parsed

def loadWord2VecModel(path):
    word2vec_model = Word2Vec.load(path)
    return word2vec_model

def loadMulticlassModelAndPredict(path, parsedUnlabelledWord2VecData):
    model_multiclass = LogisticRegressionWithLBFGS()
    model_path = "C:\\Users\\marin\\Desktop\\SparkTweets\\ClassificationModels\\"
    with gzip.open(model_path + 'MulticlassModel.pkl.gz', 'rb') as g:
        model_multiclass = cPickle.load(g)
    predictionAndLabelsMulticlass = parsedUnlabelledWord2VecData.map(lambda x: (labels[int(model_multiclass.predict(x[0].features))], x[1], x[2], x[3], x[4], x[5]))										 
    predictionAndLabelsMulticlass.pprint(20)
    return predictionAndLabelsMulticlass

def loadBinaryMulticlassRegressionModelAndPredict(model_path, classificator, parsedUnlabelledWord2VecData):
    with gzip.open(model_path + 'pol_fin' + '_' + classificator + '.pkl.gz', 'rb') as g:
        model_pol_fin = cPickle.load(g)
    #    model_pol_fin.setInitialWeights(model.weights)
    with gzip.open(model_path + 'pol_sports' + '_' + classificator + '.pkl.gz', 'rb') as g:
        model_pol_sports = cPickle.load(g)
    #    model_pol_sports.setInitialWeights(model.weights)
    with gzip.open(model_path + 'pol_tech' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_pol_tech = cPickle.load(g)
    #    model_pol_tech.setInitialWeights(model.weights)
    with gzip.open(model_path + 'pol_ent' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_pol_ent = cPickle.load(g)
     #   model_pol_ent.setInitialWeights(model.weights)
    with gzip.open(model_path + 'pol_crime' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_pol_crime = cPickle.load(g)
    #    model_pol_crime.setInitialWeights(model.weights)
    with gzip.open(model_path + 'fin_sports' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_fin_sports = cPickle.load(g)
    #    model_fin_sports.setInitialWeights(model.weights)
    with gzip.open(model_path + 'fin_tech' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_fin_tech = cPickle.load(g)
    #    model_fin_tech.setInitialWeights(model.weights)
    with gzip.open(model_path + 'fin_ent' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_fin_ent = cPickle.load(g)
    #    model_fin_ent.setInitialWeights(model.weights)
    with gzip.open(model_path + 'fin_crime' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_fin_crime = cPickle.load(g)
    #    model_fin_crime.setInitialWeights(model.weights)
    with gzip.open(model_path + 'sports_tech' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_sports_tech = cPickle.load(g)
    #    model_sports_tech.setInitialWeights(model.weights)
    with gzip.open(model_path + 'sports_ent' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_sports_ent = cPickle.load(g)
    #    model_sports_ent.setInitialWeights(model.weights)
    with gzip.open(model_path + 'sports_crime' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_sports_crime = cPickle.load(g)
     #   model_sports_crime.setInitialWeights(model.weights)
    with gzip.open(model_path + 'tech_ent' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_tech_ent = cPickle.load(g)
     #   model_tech_ent.setInitialWeights(model.weights)
    with gzip.open(model_path + 'tech_crime' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_tech_crime = cPickle.load(g)
    #    model_tech_crime.setInitialWeights(model.weights)
    with gzip.open(model_path + 'ent_crime' + '_' + classificator  + '.pkl.gz', 'rb') as g:
        model_ent_crime = cPickle.load(g)
    #    model_ent_crime.setInitialWeights(model.weights)
        
    print('=========================== PREDICTION : BINARY MULTICLASS ' + classificator + ' CLASSIFICATOR =========================================')
  
        
    predictionAndLabelsBinaryMulticlass = parsedUnlabelledWord2VecData.map(lambda r: binaryMulticlassPredict(r,
                                                 model_pol_fin,
                                                 model_pol_sports,
                                                 model_pol_tech,
                                                 model_pol_ent,
                                                 model_pol_crime,
                                                 model_fin_sports,
                                                 model_fin_tech,
                                                 model_fin_ent,
                                                 model_fin_crime,
                                                 model_sports_tech,
                                                 model_sports_ent,
                                                 model_sports_crime,
                                                 model_tech_ent,
                                                 model_tech_crime,
                                                 model_ent_crime,
                                                 labels
                                                 ))										 
    predictionAndLabelsBinaryMulticlass.pprint(20)
    return predictionAndLabelsBinaryMulticlass     

def loadCVModel(path):
	modelCV = CountVectorizerModel.load(path)
	return modelCV
	
def loadIDFModel(path):
	modelTfidf = IDFModel.load(path)
	return modelTfidf 

def loadNaiveBayesModelAndPredict(sc, path, tweetsIDF):
    model_naivebayes = NaiveBayesModel.load(sc, path)
    print('===== loadNaiveBayesModelAndPredict loaded model =====')
    predictionsAndLabelsNaiveBayes = tweetsIDF.rdd.map(lambda tweet: (tweet["_1"], model_naivebayes.predict(tweet["features"].toArray()), tweet["_2"], tweet["_3"], tweet["_5"], tweet["_6"]))
    print predictionsAndLabelsNaiveBayes.take(5)   
    return predictionsAndLabelsNaiveBayes

def createMongoDBSessionDictionary(classificatorLabels):
    dbSessionDictionary = {}
    for label in classificatorLabels:
        dbSessionDictionary[label] = SparkSession.builder.appName(label + "App").config("spark.mongodb.input.uri", "mongodb://127.0.0.1:27017/NewsTweets.News" + label).config("spark.mongodb.output.uri", "mongodb://127.0.0.1:27017/NewsTweets.News" + label).getOrCreate()
    return dbSessionDictionary

def publishPredictions(rdd, kafkaTopic):
    records = rdd.collect()
 
    for record in records:
        producer.send(kafkaTopic, record)
        producer.flush()
        
def getSparkSessionInstance(sparkConf):
    if ('sparkSessionSingletonInstance' not in globals()):
        globals()['sparkSessionSingletonInstance'] = SparkSession\
            .builder\
            .config(conf=sparkConf)\
            .getOrCreate()
    return globals()['sparkSessionSingletonInstance']

def printrdd(rdd):
    print "RDDPRINT"
    print rdd.take(5)   


if __name__ == "__main__":
    print('=========================== DEFINING LABELS =========================================')
    
    labels = ['Politics', 'Finance', 'Sports', 'Sci&Tech', 'Entertainment', 'Crime']
    
    print('=========================== LOADING SPARK CONFIGURATION =========================================')
    
    loadSparkConfigFile('C:\\Users\\marin\\Desktop\\SparkTweets\\config.properties')

    print('=========================== INITIALIZING SPARK STREAMING CONTEXT =========================================')
   
    sc = SparkContext(appName="SparkTweets")
    ssc = StreamingContext(sc, 3)   

	#Twitter Topic
    print('=========================== READING TWEETS FROM KAFKA =========================================')
   
    parsed = readTweetsFromKafkaStream(ssc, 'localhost:2181', 'spark-streaming', {'twitterTopic':1})
    tweetsIDs = parsed.map(lambda tweet: (tweet['id']))
    tweetsIDs.pprint(20)
    tweetsIDs.foreachRDD(lambda rdd: saveTweetIDsToMongo(rdd))
        
    print('================================== LOADING WORD2VEC MODEL ==============================================')
    
    num_features = 300 
    word2vec_model = loadWord2VecModel("C:\\Users\\marin\\Desktop\\SparkTweets\\Models\\lem_model")
 
    print('================================== PARSING TWEETS USING WORD2VEC ==============================================')
    
    parsedUnlabelledWord2VecData = parsed.map(lambda j: parseUnlabelledTweetsForWord2Vec(j, word2vec_model, num_features)).cache()
     
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        
    print('=========================== LOADING MODEL AND PREDICTING : MULTICLASS =========================================')
    
    predictionAndLabelsMulticlass = loadMulticlassModelAndPredict("C:\\Users\\marin\\Desktop\\SparkTweets\\ClassificationModels\\", parsedUnlabelledWord2VecData)
        
    print('================================== LOADING MODEL AND PREDICTING : BINARY MULTICLASS LOGISTIC REGRESSION ==============================================')

    predictionAndLabelsBinaryLogReg = loadBinaryMulticlassRegressionModelAndPredict("C:\\Users\\marin\\Desktop\\SparkTweets\\ClassificationModels\\", "logisticregression", parsedUnlabelledWord2VecData)
    
    print('================================== LOADING MODEL AND PREDICTING : BINARY MULTICLASS SVM ==============================================')

    predictionAndLabelsBinarySVM = loadBinaryMulticlassRegressionModelAndPredict("C:\\Users\\marin\\Desktop\\SparkTweets\\ClassificationModels\\", "svm", parsedUnlabelledWord2VecData)        
    
    print('================================== LOADING TFIDF MODEL ==============================================')

    model_idf = loadIDFModel("C:\\Users\\marin\\Desktop\\SparkTweets\\Models\\tfidf_model")        
    model_cv = loadCVModel("C:\\Users\\marin\\Desktop\\SparkTweets\\Models\\cv_model")
    
    print('================================== PARSING TWEETS USING TFIDF ==============================================')
    print type(parsed)
    #parsedUnlabelledTFIDFData = parsed.map(lambda el: test(el))
    #parsedUnlabelledTFIDFData = parsed.foreachRDD(lambda x: test(x))
    parsedUnlabelledTFIDFData = parsed.transform(lambda tweetRDD: parseUnlabelledTweetsForTFIDF(tweetRDD, model_cv, model_idf))  
    
    print('================================== LOADING MODEL AND PREDICTING : NAIVE BAYES ==============================================')
        
    predictionAnLabelsNaiveBayes = parsedUnlabelledTFIDFData.transform(lambda rdd: loadNaiveBayesModelAndPredict(sc, "C:\\Users\\marin\\Desktop\\SparkTweets\\ClassificationModels\\NaiveBayesModel", rdd))
    
    print('================================== INITIALIZING MONGO SESSION  ==============================================')
    
    classificatorLabels = ['Multiclass', 'BinaryLogisticRegression', 'BinarySVM', 'NaiveBayes']
    
    dbSessionDictionary = createMongoDBSessionDictionary(classificatorLabels)

    print('================================== SAVING PREDICTIONS TO MONGO DB  ==============================================')
    
    predictionAndLabelsMulticlass.foreachRDD(lambda rdd: saveToMongo(rdd, dbSessionDictionary[classificatorLabels[0]], classificatorLabels[0]))
    predictionAndLabelsBinaryLogReg.foreachRDD(lambda rdd: saveToMongo(rdd, dbSessionDictionary[classificatorLabels[1]], classificatorLabels[1]))
    predictionAndLabelsBinarySVM.foreachRDD(lambda rdd: saveToMongo(rdd, dbSessionDictionary[classificatorLabels[2]], classificatorLabels[2]))
    predictionAnLabelsNaiveBayes.foreachRDD(lambda rdd: saveToMongo(rdd, dbSessionDictionary[classificatorLabels[3]], classificatorLabels[3]))
   
    print('================================== MERGING PREDICTIONS  ==============================================')
    
    stream1 = predictionAndLabelsMulticlass.map(lambda el: (el[2], el))
    stream2 = predictionAndLabelsBinaryLogReg.map(lambda el: (el[2], el))
    stream3 = predictionAndLabelsBinarySVM.map(lambda el: (el[2], el))
    stream4 = predictionAnLabelsNaiveBayes.map(lambda el: (el[0], el))
    
    print('================================== stream3  ==============================================')
    print type(stream3)
    print('================================== stream4  ==============================================')
    print type(stream4)
    
    joined = stream1.join(stream2).join(stream3).join(stream4).cache()
    print('================================== parsed  ==============================================')
    print type(joined)
    print('================================== joined  ==============================================')
    #joined.pprint(20)
    #joined.foreachRDD(lambda rdd: printrdd(rdd))
    #joinedMap = joined.transform(lambda (el, (((c1, c2), c3), c4)) : createJson(el, c1[1], c1[3], c1[0], c2[0][0], c3[0][0], labels[int(c4[1])]))
    joinedMap = joined.map(lambda (el, ((((c1), (c2)), (c3)), (c4))) : createJson(el, c1[1], c1[3], c1[0], c2[0][0], c3[0][0], labels[int(c4[1])]))
    
    print('================================== PUBLISHING TO PREDICTIONS KAFKA TOPIC  ==============================================')
    #print type(joinedMap)
    joinedMap.foreachRDD(lambda rdd: publishPredictions(rdd, 'predictions_topic'))
    #publishPredictions(joinedMap, 'predictions_topic')   
        
    ssc.start()
    ssc.awaitTermination()